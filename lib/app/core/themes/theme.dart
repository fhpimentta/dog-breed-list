import 'package:flutter/material.dart';
class AppTheme{
  


  //Colors for theme
//  Color(0xfffcfcff);
  static Color lightPrimary = Color(0xfffcfcff);
  static Color darkPrimary = Colors.black;
  static Color lightAccent = Color.fromRGBO(140, 198, 63, 1);
  static Color lightAccentSecondary = Color.fromRGBO(108, 176, 39, 1);
  static Color darkAccent = Color.fromRGBO(140, 198, 63, 1);
  static Color lightBG = Color(0xfffcfcff);
  static Color darkBG = Colors.black;
  static Color ratingBG = Colors.yellow[600];
  
  static Color textColor = Color.fromRGBO(151, 165, 191, 1);
  static Color white = Colors.white;
  static Color borderColor = Color.fromRGBO(235, 236, 237, 1);
  static Color lightAccentBlue = Color.fromRGBO(46, 174, 241, 1);
  
  //140 198 63
  //108 176 39
  
  static Color red = Colors.red;

  
  
  

  static Color textColor2 = Color.fromRGBO(73, 84, 100, 1);
  static Color appBarSchedule = Color.fromRGBO(21, 105, 233, 1);
  


  static ThemeData lightTheme = ThemeData(
    
    
    
    focusColor: darkPrimary,
    canvasColor: textColor2,
    hintColor: textColor,
    backgroundColor: lightBG,
    primaryColor: lightPrimary,
    primarySwatch: Colors.green,
    accentColor:  lightAccent,
    cursorColor: lightAccent,
    scaffoldBackgroundColor: lightBG,
    appBarTheme: AppBarTheme(
      textTheme: TextTheme(
        title: TextStyle(
          color: darkBG,
          fontSize: 18.0,
          fontWeight: FontWeight.w800,
        ),
      ),
//      iconTheme: IconThemeData(
//        color: lightAccent,
//      ),
    ),
  );

  static ThemeData darkTheme = ThemeData(
    
    brightness: Brightness.dark,
    backgroundColor: darkBG,
    primaryColor: darkPrimary,
    accentColor: darkAccent,
    scaffoldBackgroundColor: darkBG,
    cursorColor: darkAccent,
    appBarTheme: AppBarTheme(
      textTheme: TextTheme(
        title: TextStyle(
          color: lightBG,
          fontSize: 18.0,
          fontWeight: FontWeight.w800,
        ),
      ),
//      iconTheme: IconThemeData(
//        color: darkAccent,
//      ),
    ),
  );


}
