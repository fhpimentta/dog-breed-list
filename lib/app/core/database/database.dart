


abstract class Database {

  Future  get(table);

  Future save(table,values);

  Future addToList(table,value);

  Future clean();

  Future saveRow(table,value);

  Future getRow(table,query);

  Future saveBool(table, values);

  Future getBool(table);
}