import 'package:dogbreed/app/core/models/breed_model.dart';
import 'package:dogbreed/app/modules/home/home_controller.dart';
import 'package:dogbreed/app/modules/home/repository/create_breed_repository.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'new_breed_controller.g.dart';

@Injectable()
class NewBreedController = _NewBreedControllerBase with _$NewBreedController;

abstract class _NewBreedControllerBase with Store {
 
    CreateBreedRepository createBreedRepository = CreateBreedRepository();


 @observable
 String name;

 @action
 nameChange(String value) => name = value;


 @observable
 String description;

 @action
 descriptionChange(String value) => description = value;


  @computed 
  bool get nameValidate => name!=null && name!= "" ?name.length>1:true;

 @computed 
  bool get descriptionValidade => description!=null && description!= "" ?description.length>5:true;



    @computed 
  bool get canSubmit => 
  nameValidate && 
  descriptionValidade;

  @computed 
  bool get noZero => 
  nameValidate && 
  descriptionValidade;

 @action
  submit(context)async{
    if( canSubmit && noZero){
        HomeController ctr = Modular.get();
        final breed = BreedModel(
          id:ctr.list.length,
          name: name,
          description: description
        );
       await createBreedRepository.create(breed:breed );

        ctr.add(breed:breed);
        Modular.to.pop();
    }
  }
}
