// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_breed_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $NewBreedController = BindInject(
  (i) => NewBreedController(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NewBreedController on _NewBreedControllerBase, Store {
  Computed<bool> _$nameValidateComputed;

  @override
  bool get nameValidate =>
      (_$nameValidateComputed ??= Computed<bool>(() => super.nameValidate,
              name: '_NewBreedControllerBase.nameValidate'))
          .value;
  Computed<bool> _$descriptionValidadeComputed;

  @override
  bool get descriptionValidade => (_$descriptionValidadeComputed ??=
          Computed<bool>(() => super.descriptionValidade,
              name: '_NewBreedControllerBase.descriptionValidade'))
      .value;
  Computed<bool> _$canSubmitComputed;

  @override
  bool get canSubmit =>
      (_$canSubmitComputed ??= Computed<bool>(() => super.canSubmit,
              name: '_NewBreedControllerBase.canSubmit'))
          .value;
  Computed<bool> _$noZeroComputed;

  @override
  bool get noZero => (_$noZeroComputed ??= Computed<bool>(() => super.noZero,
          name: '_NewBreedControllerBase.noZero'))
      .value;

  final _$nameAtom = Atom(name: '_NewBreedControllerBase.name');

  @override
  String get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  final _$descriptionAtom = Atom(name: '_NewBreedControllerBase.description');

  @override
  String get description {
    _$descriptionAtom.reportRead();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.reportWrite(value, super.description, () {
      super.description = value;
    });
  }

  final _$submitAsyncAction = AsyncAction('_NewBreedControllerBase.submit');

  @override
  Future submit(dynamic context) {
    return _$submitAsyncAction.run(() => super.submit(context));
  }

  final _$_NewBreedControllerBaseActionController =
      ActionController(name: '_NewBreedControllerBase');

  @override
  dynamic nameChange(String value) {
    final _$actionInfo = _$_NewBreedControllerBaseActionController.startAction(
        name: '_NewBreedControllerBase.nameChange');
    try {
      return super.nameChange(value);
    } finally {
      _$_NewBreedControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic descriptionChange(String value) {
    final _$actionInfo = _$_NewBreedControllerBaseActionController.startAction(
        name: '_NewBreedControllerBase.descriptionChange');
    try {
      return super.descriptionChange(value);
    } finally {
      _$_NewBreedControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
name: ${name},
description: ${description},
nameValidate: ${nameValidate},
descriptionValidade: ${descriptionValidade},
canSubmit: ${canSubmit},
noZero: ${noZero}
    ''';
  }
}
