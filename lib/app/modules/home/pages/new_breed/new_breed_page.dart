import 'package:dogbreed/app/widgets/custom_button/custom_button_controller.dart';
import 'package:dogbreed/app/widgets/custom_button/custom_button_widget.dart';
import 'package:dogbreed/app/widgets/custom_input/custom_input_controller.dart';
import 'package:dogbreed/app/widgets/custom_input/custom_input_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'new_breed_controller.dart';

class NewBreedPage extends StatefulWidget {
  final String title;
  const NewBreedPage({Key key, this.title = "Adicionar"}) : super(key: key);

  @override
  _NewBreedPageState createState() => _NewBreedPageState();
}

class _NewBreedPageState
    extends ModularState<NewBreedPage, NewBreedController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title,style:TextStyle(color:Theme.of(context).accentColor)),
      ),
      body: Column(
        children: <Widget>[
           Observer(
                builder: (_) {
                  return CustomInputWidget(
                    controller: CustomInputController(
                        label: "Nome",
                        errorText: "Nome muito curto",
                        icon: Icons.pets,
                        callback: controller.nameChange,
                        txtType: TextInputType.text,
                        isValid: controller.nameValidate),
                  );
                },
              ),
               Observer(
                builder: (_) {
                  return CustomInputWidget(
                    controller: CustomInputController(
                        label: "Descrição",
                        errorText: "Descrição muito curto",
                        icon: Icons.description,
                        callback: controller.descriptionChange,
                        txtType: TextInputType.text,
                        isValid: controller.descriptionValidade),
                  );
                },
              ),
              CustomButtonWidget(
                controller: CustomButtonController(label: 'Salvar',callback: (){controller.submit(context);}),
              ),
        ],
      ),
    );
  }




  


}
