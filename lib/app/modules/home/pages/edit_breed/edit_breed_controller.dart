import 'package:dogbreed/app/core/models/breed_model.dart';
import 'package:dogbreed/app/modules/home/home_controller.dart';
import 'package:dogbreed/app/modules/home/repository/remove_breed_repository.dart';
import 'package:dogbreed/app/modules/home/repository/update_breed_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'edit_breed_controller.g.dart';

@Injectable()
class EditBreedController = _EditBreedControllerBase with _$EditBreedController;

abstract class _EditBreedControllerBase with Store {
 
UpdateBreedRepository updateBreedRepository = UpdateBreedRepository();
RemoveBreedRepository removeBreedRepository = RemoveBreedRepository();

 BreedModel _breed;
 
 @observable
 String name;

 @action
 nameChange(String value) => name = value;


 @observable
 String description;

 @action
 descriptionChange(String value) => description = value;


  @computed 
  bool get nameValidate => name!=null && name!= "" ?name.length>1:true;

 @computed 
  bool get descriptionValidade => description!=null && description!= "" ?description.length>5:true;

  var nameCtr = TextEditingController();
  var descriptionCtr = TextEditingController();


  init(breed){
    _breed = breed;
    nameCtr.text = _breed.name;
    descriptionCtr.text = _breed.description;
    name = _breed.name;
    description = _breed.description;

  }

  @computed 
  bool get canSubmit => 
  nameValidate && 
  descriptionValidade;

  @computed 
  bool get noZero => 
  nameValidate && 
  descriptionValidade;

 @action
  submit(context)async{
    if( canSubmit && noZero){
        HomeController ctr = Modular.get();
        _breed.name = name;
        _breed.description = description;
        updateBreedRepository.update(breed: _breed);
        ctr.update(breed: _breed);
        Modular.to.pop();
    }
  }

  @action 
  remove(context)async{
      HomeController ctr = Modular.get();
        removeBreedRepository.remove(_breed.id);
        ctr.remove(id: _breed.id);
        Modular.to.pop();
  }


}
