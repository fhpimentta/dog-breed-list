import 'package:dogbreed/app/core/models/breed_model.dart';
import 'package:dogbreed/app/widgets/custom_button/custom_button_controller.dart';
import 'package:dogbreed/app/widgets/custom_button/custom_button_widget.dart';
import 'package:dogbreed/app/widgets/custom_input/custom_input_controller.dart';
import 'package:dogbreed/app/widgets/custom_input/custom_input_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'edit_breed_controller.dart';

class EditBreedPage extends StatefulWidget {
  final BreedModel breed;
  const EditBreedPage({Key key, @required this.breed }) : super(key: key);

  @override
  _EditBreedPageState createState() => _EditBreedPageState();
}

class _EditBreedPageState
    extends ModularState<EditBreedPage, EditBreedController> {
  //use 'controller' variable to access controller


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller.init(widget.breed);
  }

 @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.breed.name,style:TextStyle(color:Theme.of(context).accentColor)),
      ),
      body: Stack(
        children: [
          Column(
            children: <Widget>[
               Observer(
                    builder: (_) {
                      return CustomInputWidget(
                        controller: CustomInputController(
                          ctr: controller.nameCtr,
                            label: "Nome",
                            errorText: "Nome muito curto",
                            icon: Icons.pets,
                            callback: controller.nameChange,
                            txtType: TextInputType.text,
                            isValid: controller.nameValidate),
                      );
                    },
                  ),
                   Observer(
                    builder: (_) {
                      return CustomInputWidget(
                        controller: CustomInputController(
                          ctr:controller.descriptionCtr,
                            label: "Descrição",
                            errorText: "Descrição muito curto",
                            icon: Icons.description,
                            callback: controller.descriptionChange,
                            txtType: TextInputType.text,
                            isValid: controller.descriptionValidade),
                      );
                    },
                  ),
                  CustomButtonWidget(
                    controller: CustomButtonController(label: 'Atualizar',callback: (){controller.submit(context);}),
                  ),
            ],
          ),

          Align(
            alignment: Alignment.bottomCenter,
            child: CustomButtonWidget(
                controller: CustomButtonController(
                  label: 'Apagar',
                  callback: (){controller.remove(context);},
                  color: Colors.red
                  ),
              ),
          )
        ],
      ),
    );
  }

}
