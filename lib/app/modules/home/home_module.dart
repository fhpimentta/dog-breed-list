import 'package:dogbreed/app/modules/home/pages/edit_breed/edit_breed_page.dart';

import 'pages/edit_breed/edit_breed_controller.dart';
import 'package:dogbreed/app/modules/home/pages/new_breed/new_breed_page.dart';

import 'pages/new_breed/new_breed_controller.dart';
import 'tiles/breed_tile/breed_tile_controller.dart';
import 'repository/get_breeds_repository.dart';
import 'repository/create_breed_repository.dart';
import 'home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'home_page.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $EditBreedController,
        $NewBreedController,
        $BreedTileController,
        $GetBreedsRepository,
        $CreateBreedRepository,
        $HomeController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => HomePage()),
        ModularRouter('/add', child: (_, args) => NewBreedPage()),
        ModularRouter('/edit',
            child: (_, args) => EditBreedPage(
                  breed: args.data,
                )),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
