import 'package:dogbreed/app/core/models/breed_model.dart';
import 'package:dogbreed/app/modules/home/repository/create_breed_repository.dart';
import 'package:dogbreed/app/modules/home/repository/get_breeds_repository.dart';
import 'package:dogbreed/app/modules/home/tiles/breed_tile/breed_tile_controller.dart';
import 'package:flutter/foundation.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'home_controller.g.dart';

@Injectable()
class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
 
  GetBreedsRepository getBreedsRepository = GetBreedsRepository();



  @observable
  ObservableList<BreedTileController> list = [BreedTileController(breed: BreedModel())].asObservable();


  @observable
    var filter = "";

  @action
  setFileter(String value) => filter = value;


  @computed
    List<BreedTileController> get listFilter {
      if(filter.isEmpty){
        return list;
      }else{
        return list.where((i)=> i.breed.name.toLowerCase().contains(filter.toLowerCase())).toList();
      }

    }


  init()async{
    list.clear();
    (await getBreedsRepository.get()).forEach((element) {
      list.add(BreedTileController(breed: BreedModel.fromJson(element)));
    });
  }


  @action
  add({@required BreedModel breed}){
    list.add(BreedTileController(breed: breed));
  }

   @action
  update({@required BreedModel breed}){
   var index = list.indexWhere((element) => element.breed.id == breed.id);
   list[index] = BreedTileController(breed: breed);
   
  }

  @action 
  remove({@required int id}){
    list.removeWhere((element) => element.breed.id == id);
  }
}
