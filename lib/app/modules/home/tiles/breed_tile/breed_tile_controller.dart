import 'package:dogbreed/app/core/models/breed_model.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'breed_tile_controller.g.dart';

@Injectable()
class BreedTileController = _BreedTileControllerBase with _$BreedTileController;

abstract class _BreedTileControllerBase with Store {
  @observable
  BreedModel breed;
  

  _BreedTileControllerBase({@required this.breed});
  
}
