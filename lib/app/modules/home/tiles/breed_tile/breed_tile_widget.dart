import 'package:dogbreed/app/modules/home/tiles/breed_tile/breed_tile_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

class BreedTileWidget extends StatelessWidget {
  
  BreedTileController controller;

  BreedTileWidget({@required this.controller});
  
  
  @override
  Widget build(BuildContext context) {
    return Card(
       color: Theme.of(context).accentColor,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor
              ),
        height: ScreenUtil().setHeight(200),
        child: Wrap(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(flex:2,child:Text(controller.breed.name,style: TextStyle(color: Theme.of(context).primaryColor,fontSize: ScreenUtil().setSp(35)),)),
                SizedBox(width: 2,),
                Expanded(flex:5,child:Text(controller.breed.description,style: TextStyle(color: Theme.of(context).primaryColor,fontSize: ScreenUtil().setSp(30)),)),
              ],
            ),
          ],
        ),
      ),
          ),
    );
  }
}
