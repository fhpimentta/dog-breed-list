// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'breed_tile_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $BreedTileController = BindInject(
  (i) => BreedTileController(breed: i<BreedModel>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BreedTileController on _BreedTileControllerBase, Store {
  final _$breedAtom = Atom(name: '_BreedTileControllerBase.breed');

  @override
  BreedModel get breed {
    _$breedAtom.reportRead();
    return super.breed;
  }

  @override
  set breed(BreedModel value) {
    _$breedAtom.reportWrite(value, super.breed, () {
      super.breed = value;
    });
  }

  @override
  String toString() {
    return '''
breed: ${breed}
    ''';
  }
}
