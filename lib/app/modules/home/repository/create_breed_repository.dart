import 'package:dogbreed/app/app_controller.dart';
import 'package:dogbreed/app/core/constants/database.dart';
import 'package:dogbreed/app/core/models/breed_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'create_breed_repository.g.dart';

@Injectable()
class CreateBreedRepository extends Disposable {

  Future create({@required BreedModel breed}) async {
   AppController app = Modular.get();
    await app.database.addToList(BREED,breed.toJson());
  
  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
