import 'package:dogbreed/app/app_controller.dart';
import 'package:dogbreed/app/core/constants/database.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/native_imp.dart';


class RemoveBreedRepository extends Disposable {
  
  Future remove(id) async {
   
  AppController app = Modular.get();
   var breeds =  await app.database.get(BREED);
   breeds.removeWhere((element) => element['id']== id);
   
   await app.database.save(BREED, breeds);


  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
