import 'package:dogbreed/app/app_controller.dart';
import 'package:dogbreed/app/core/constants/database.dart';
import 'package:dogbreed/app/core/models/breed_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_modular/flutter_modular.dart';



class UpdateBreedRepository extends Disposable {


  Future update({@required BreedModel breed}) async {
    AppController app = Modular.get();
   var breeds =  await app.database.get(BREED);
   var index = breeds.indexWhere((element) => element['id'] == breed.id);
   breeds[index] = breed.toJson();
   await app.database.save(BREED, breeds);
  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
