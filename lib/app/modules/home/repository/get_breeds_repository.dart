import 'package:dogbreed/app/app_controller.dart';
import 'package:dogbreed/app/core/constants/database.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/native_imp.dart';

part 'get_breeds_repository.g.dart';

@Injectable()
class GetBreedsRepository extends Disposable {


  Future<List> get() async {
    AppController app = Modular.get();
    var p = await app.database.get(BREED);
    return p;
  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
