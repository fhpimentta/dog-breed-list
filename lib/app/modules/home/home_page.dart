import 'package:dogbreed/app/modules/home/tiles/breed_tile/breed_tile_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {

  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller.init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Raças",style:TextStyle(color:Theme.of(context).accentColor)),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add,color:Theme.of(context).primaryColor),
        onPressed: (){
          Modular.to.pushNamed('/breeds/add');
        }
      ),
      body: body(),
    );
  }



  body() {
    return Stack(
      children: [
        Align(
          alignment: Alignment.topCenter,
          child: Column(children: [
            searchBreed(),
             Expanded(
              child: Observer(
                builder: (_){
                  return Padding(
                    padding: const EdgeInsets.only(top: 20,bottom: 10),
                    child: ListView.separated(
                itemCount: controller.listFilter.length,
                itemBuilder: (_,index){
                    return Slidable(
                      actionPane: SlidableDrawerActionPane(),
                      secondaryActions: [
                         IconSlideAction(
                          onTap: () {
                           Modular.to.pushNamed('/breeds/edit',arguments:controller.listFilter[index].breed );
                          },
                          color: Theme.of(context).accentColor,
                          iconWidget: Icon(Icons.assignment_late_outlined ,color:Colors.white),

                        ), 
                
                      ],
                      child: BreedTileWidget(controller:controller.listFilter[index] ,)
                    );
              
                }, separatorBuilder: (BuildContext context, int index) { 
                  return Container(height: 1,color: Colors.black12,);
                },


              ),
                  );
                },
              )
            
            ),
          ],),
        ),
        
       
      ],
    );
  }

  searchBreed() {
    return Container(
      decoration: BoxDecoration(
        
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      width: ScreenUtil().setWidth(800),
      height: ScreenUtil().setHeight(110),
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: inputText(labelText: "Procurar"),
        ),
    );
  }

  Widget inputText({labelText}) {
    return TextField(
      onChanged: controller.setFileter,
        style: GoogleFonts.lato(
          fontSize: 15,
          color: Theme.of(context).accentColor
        ),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            hintText: labelText,
            border: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Theme.of(context).accentColor, width: 32.0),
                borderRadius: BorderRadius.circular(25.0)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Theme.of(context).accentColor, width: 1.0),
                borderRadius: BorderRadius.circular(25.0)),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              borderSide: BorderSide(width: 1, color: Theme.of(context).accentColor),
            )));
  }


}
