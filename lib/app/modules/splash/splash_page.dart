import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'splash_controller.dart';

class SplashPage extends StatefulWidget {
  final String title;
  const SplashPage({Key key, this.title = "Splash"}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends ModularState<SplashPage, SplashController> {
  //use 'controller' variable to access controller

   @override
  void initState() {
    super.initState();
    controller.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
  
      body: Stack(
        children: [
          Center(
              child: Container(
                width: 300,
                height: 300,
                child: Image.asset('assets/icons/icon.png',)),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child:Padding(
            padding: const EdgeInsets.all(80.0),
            child: CircularProgressIndicator(),
          )
          
          )
        ],
      )
    );
  }

}
