import 'package:flutter/painting.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mobx/mobx.dart';

part 'splash_controller.g.dart';

@Injectable()
class SplashController = _SplashControllerBase with _$SplashController;

abstract class _SplashControllerBase with Store {
  init(context)async{
    await Future.delayed(Duration(seconds: 4));
    ScreenUtil.init(context, designSize: Size(750, 1334), allowFontScaling: false);
    Modular.to.pushReplacementNamed('/breeds');
  }
}
