import 'package:dogbreed/app/widgets/custom_input/custom_input_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class CustomInputWidget extends StatelessWidget {
  
 final CustomInputController controller;
 
  CustomInputWidget({
    Key key,
    this.controller,
  }) : super(key: key);
 
 

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16,right: 16,top: 10),
      child: Observer(
        builder: (_){
          return TextField(
            controller: controller.ctr,
              keyboardType: controller.txtType,
              onChanged: controller.inputChange,
              style: TextStyle(
                fontSize: 14.0,
                color: Theme.of(context).accentColor,
              ),
              decoration: InputDecoration(
                  errorText: controller.isValid?null:controller.errorText,
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  prefixIcon: Icon(controller.icon,color: Theme.of(context).accentColor,),
  
                  hintText: controller.label,
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Theme.of(context).accentColor, width: 32.0),
                      borderRadius: BorderRadius.circular(25.0)),
                  focusedBorder: OutlineInputBorder(

                      borderSide: BorderSide(color: Theme.of(context).accentColor, width: 1.0),
                      borderRadius: BorderRadius.circular(25.0)
                      
                      )
                      
                      )
                      
                      );
        },
      ),
    );
  }
}
