import 'package:flutter/foundation.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'custom_input_controller.g.dart';

@Injectable()
class CustomInputController = _CustomInputControllerBase
    with _$CustomInputController;

abstract class _CustomInputControllerBase with Store {
  
  @observable
  var errorText;

  @observable
  var label;

  @observable  
  var icon;

  @observable 
  var callback;

  @observable
  var  isValid;

  @observable
  var ctr; 

  @observable 
  var txtType;


  @action 
  inputChange(value)=>callback(value);


_CustomInputControllerBase({
 @required this.label,
 @required this.errorText,
 @required this.icon,
 @required this.callback,
 @required this.isValid,
 @required this.ctr,
 @required this.txtType,
 
 });
}
