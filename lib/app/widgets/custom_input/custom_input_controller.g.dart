// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'custom_input_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $CustomInputController = BindInject(
  (i) => CustomInputController(
      label: i<dynamic>(),
      errorText: i<dynamic>(),
      icon: i<dynamic>(),
      callback: i<dynamic>(),
      isValid: i<dynamic>(),
      ctr: i<dynamic>(),
      txtType: i<dynamic>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CustomInputController on _CustomInputControllerBase, Store {
  final _$errorTextAtom = Atom(name: '_CustomInputControllerBase.errorText');

  @override
  dynamic get errorText {
    _$errorTextAtom.reportRead();
    return super.errorText;
  }

  @override
  set errorText(dynamic value) {
    _$errorTextAtom.reportWrite(value, super.errorText, () {
      super.errorText = value;
    });
  }

  final _$labelAtom = Atom(name: '_CustomInputControllerBase.label');

  @override
  dynamic get label {
    _$labelAtom.reportRead();
    return super.label;
  }

  @override
  set label(dynamic value) {
    _$labelAtom.reportWrite(value, super.label, () {
      super.label = value;
    });
  }

  final _$iconAtom = Atom(name: '_CustomInputControllerBase.icon');

  @override
  dynamic get icon {
    _$iconAtom.reportRead();
    return super.icon;
  }

  @override
  set icon(dynamic value) {
    _$iconAtom.reportWrite(value, super.icon, () {
      super.icon = value;
    });
  }

  final _$callbackAtom = Atom(name: '_CustomInputControllerBase.callback');

  @override
  dynamic get callback {
    _$callbackAtom.reportRead();
    return super.callback;
  }

  @override
  set callback(dynamic value) {
    _$callbackAtom.reportWrite(value, super.callback, () {
      super.callback = value;
    });
  }

  final _$isValidAtom = Atom(name: '_CustomInputControllerBase.isValid');

  @override
  dynamic get isValid {
    _$isValidAtom.reportRead();
    return super.isValid;
  }

  @override
  set isValid(dynamic value) {
    _$isValidAtom.reportWrite(value, super.isValid, () {
      super.isValid = value;
    });
  }

  final _$ctrAtom = Atom(name: '_CustomInputControllerBase.ctr');

  @override
  dynamic get ctr {
    _$ctrAtom.reportRead();
    return super.ctr;
  }

  @override
  set ctr(dynamic value) {
    _$ctrAtom.reportWrite(value, super.ctr, () {
      super.ctr = value;
    });
  }

  final _$txtTypeAtom = Atom(name: '_CustomInputControllerBase.txtType');

  @override
  dynamic get txtType {
    _$txtTypeAtom.reportRead();
    return super.txtType;
  }

  @override
  set txtType(dynamic value) {
    _$txtTypeAtom.reportWrite(value, super.txtType, () {
      super.txtType = value;
    });
  }

  final _$_CustomInputControllerBaseActionController =
      ActionController(name: '_CustomInputControllerBase');

  @override
  dynamic inputChange(dynamic value) {
    final _$actionInfo = _$_CustomInputControllerBaseActionController
        .startAction(name: '_CustomInputControllerBase.inputChange');
    try {
      return super.inputChange(value);
    } finally {
      _$_CustomInputControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
errorText: ${errorText},
label: ${label},
icon: ${icon},
callback: ${callback},
isValid: ${isValid},
ctr: ${ctr},
txtType: ${txtType}
    ''';
  }
}
