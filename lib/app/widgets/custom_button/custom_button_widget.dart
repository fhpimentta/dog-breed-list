import 'package:dogbreed/app/widgets/custom_button/custom_button_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomButtonWidget extends StatelessWidget {
 
 final CustomButtonController controller;
 CustomButtonWidget({@required this.controller});
 
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(36.0),
      child: GestureDetector(
            onTap: controller.callback,
            child: Container(
            
            height: ScreenUtil().setHeight(100),
            decoration: BoxDecoration(
                color: controller.color==null? Theme.of(context).accentColor:controller.color,
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Center(child: Text(controller.label,style:TextStyle(  color: Theme.of(context).primaryColor,))),
        ),
      ),
    );
  }
}
