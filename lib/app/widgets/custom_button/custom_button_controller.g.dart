// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'custom_button_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $CustomButtonController = BindInject(
  (i) => CustomButtonController(
      label: i<String>(), callback: i<Function>(), color: i<Color>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CustomButtonController on _CustomButtonControllerBase, Store {
  final _$labelAtom = Atom(name: '_CustomButtonControllerBase.label');

  @override
  String get label {
    _$labelAtom.reportRead();
    return super.label;
  }

  @override
  set label(String value) {
    _$labelAtom.reportWrite(value, super.label, () {
      super.label = value;
    });
  }

  final _$colorAtom = Atom(name: '_CustomButtonControllerBase.color');

  @override
  Color get color {
    _$colorAtom.reportRead();
    return super.color;
  }

  @override
  set color(Color value) {
    _$colorAtom.reportWrite(value, super.color, () {
      super.color = value;
    });
  }

  final _$callbackAtom = Atom(name: '_CustomButtonControllerBase.callback');

  @override
  Function get callback {
    _$callbackAtom.reportRead();
    return super.callback;
  }

  @override
  set callback(Function value) {
    _$callbackAtom.reportWrite(value, super.callback, () {
      super.callback = value;
    });
  }

  @override
  String toString() {
    return '''
label: ${label},
color: ${color},
callback: ${callback}
    ''';
  }
}
