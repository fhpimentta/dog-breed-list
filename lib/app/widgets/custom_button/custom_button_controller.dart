import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'custom_button_controller.g.dart';

@Injectable()
class CustomButtonController = _CustomButtonControllerBase
    with _$CustomButtonController;

abstract class _CustomButtonControllerBase with Store {
  @observable
  String label;

  @observable
  Color color;

  @observable
  Function callback;

  _CustomButtonControllerBase({@required this.label,@required this.callback,this.color});

 
}
