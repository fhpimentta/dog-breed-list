import 'package:dogbreed/app/core/constants/database.dart';
import 'package:dogbreed/app/core/database/database.dart';
import 'package:dogbreed/app/core/database/sharedpreferences.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'app_controller.g.dart';

@Injectable()
class AppController = _AppControllerBase with _$AppController;

abstract class _AppControllerBase with Store {
  bool isDarkTheme = false;

 Database database = SharedPreferencesDB(); 

  init()async{

    var themeDB = await database.getBool(THEMEISDARK);
    if(themeDB != null){
      isDarkTheme = themeDB;
    }
  }

}
