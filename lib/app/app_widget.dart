import 'package:dogbreed/app/app_controller.dart';
import 'package:dogbreed/app/core/themes/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_i18n/flutter_i18n_delegate.dart';
import 'package:flutter_i18n/loaders/file_translation_loader.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'core/constants/app.dart';

class AppWidget extends StatelessWidget {
    AppController controller = Modular.get();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

   return Observer(
      builder: (_){
        return  MaterialApp(
          localizationsDelegates: [
               FlutterI18nDelegate(
                translationLoader: FileTranslationLoader(        
                  useCountryCode: false,
                  fallbackFile: 'pt',
                  basePath: 'assets/lang',
                  forcedLocale: Locale('pt')),
                 missingTranslationHandler: (key, locale) {
                  print("--- Missing Key: $key, languageCode: ${locale.languageCode}");
                },
        ),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('pt'),
          ],
          builder: FlutterI18n.rootAppBuilder() ,//If you want to support RTL.
          navigatorKey: Modular.navigatorKey,
          title: APP_NAME,
          debugShowCheckedModeBanner: false,
          theme: controller.isDarkTheme?AppTheme.darkTheme:AppTheme.lightTheme,
          initialRoute: '/',
          onGenerateRoute: Modular.generateRoute,

        );
      },
    );
  }
}
