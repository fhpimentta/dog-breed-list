<h1 align="center">
	<img alt="DogBreed" src="/assets/icons/icon.png" width="200px" />
</h1>

<h3 align="center">
  Dog Breed
</h3>



## 📥 Instalação e execução

1. Faça um clone desse repositório.
2. Instalar <a href='https://flutter.dev/docs/get-started/install'>Flutter</a>
2. Instalar <a href='https://github.com/Flutterando/slidy'>Slidy</a> ( opcional )


### Backend

1. A partir da raiz do projeto, entre na pasta do backend rodando `flutter pub get`;



Feito com ♥ by [FernandoPimenta](https://www.linkedin.com/in/fepimenta/)
